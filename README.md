HealthProfileSolution -- IntroSDE@UniTN
============

A semester project of [Introduction to Service Design and Engineering][1], developed by [Ans Riaz][2] and [Jasim Khan][3], the students of the [University of Trento, Italy][4].

HealthProfileSolution is basically a lifestyle coach system, that monitors your exercises, diet or prescription advised by your doctor. It suggests you about your exercises, weather forecasting, daily report about your diet and health activities. 

It has a server side, in which both SOAP and REST services implemented, and a client side which is a command line client to communicate with server. [Twitter share][10], [Forismatic quotes][11] and [OpenWeathr][12] is used for sharing your life status on Twitter, inspirational quotes and weather forecasting respectively.  

## Application Architecture

Application architecture can be seen [here][5]

## API Documentation

[API documentation][19] is hosted on Apiary.com

Other JavaDoc documentation is also available in the project [doc folder][20]

## Modules Index

* **[OpenWeather][6]**: It communicates with OpenWeather to get forecast about weather from OpenWeather.   

* **[Twitter4j][7]**: It communicates with Twitter to share tweets. 

* **[Forismatic][8]**: It communicates with Forismatic quotes services to get random inspirational quotes.

* **[PersistenceFunctions][9]**:  It has all the functions that communicates with database using Eclipse Persistence Service. We make it to keep the model classes clean and easy to understandable.

* **[HealthProfileSolution][16]**:  In this class, we wrote all the services that are used to communicate with SOAP server for example, login, logout, read profile etc. [shareOnTwitter][17] and [persuasiveStrategy][18] are the main function (orchestration) in which we are communicating with REST server and external and internal datasources.

## Team members

[Ans Riaz][2]: Mostly worked on server side, implements REST and SOAP services. Implements [Twitter share][10], [Forismatic quotes service][11] and [OpenWeather][12] to get the quotes and weather forecast. 

[Jasim Khan][3]: Mostly worked on client side, database and also server side. Implements client, contributes in server side SOAP services. Implements polling function to get the push about recent goal.

## How to run

First run the REST server by running the file [App.java][13] as Java Application, then run SOAP server by running the file [Endpoint - HealthPublisher.java][14] as Java Application. After this run the [client][15]. 

[1]: https://sites.google.com/site/introsdeunitn/
[2]: http://about.me/rizh
[3]: https://www.facebook.com/JASIM.KHAN.AFRIDI
[4]: http://www.unitn.it/
[5]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/Flowchart/?at=master
[6]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/external/weather/OpenWeather.java?at=master
[7]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/external/twitter/UpdateStatus.java?at=master
[8]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/external/forismatic/GetForismaticQuotes.java?at=master
[9]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/model/PersistanceFunctions.java?at=master
[10]: http://twitter4j.org/en/code-examples.html
[11]: http://forismatic.com/en/
[12]: http://openweathermap.org/
[13]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/App.java?at=master
[14]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/document/endpoint/EhealthPublisher.java?at=master
[15]: https://bitbucket.org/khan990/healthprofileclient
[16]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/document/ws/HealthProfileSolution.java?at=master
[17]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/document/ws/HealthProfileSolutionImpl.java?at=master#cl-551
[18]: https://bitbucket.org/introsde/healthprofilesolution/src/3e9d1396b5147302a26b59216a697e406d572702/src/ehealth/document/ws/HealthProfileSolutionImpl.java?at=master#cl-319
[19]: http://docs.rizhch.apiary.io/
[20]: https://bitbucket.org/introsde/healthprofilesolution/src/258ac8bdca438f032e3508db61c2f8f98a59aa59/doc/?at=master