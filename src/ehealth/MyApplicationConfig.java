package ehealth;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * <p>
 * Configration file for the REST server.
 * <p>
 * 
 * @author Ans Riaz
 *
 */
@ApplicationPath("healthsolutions")
public class MyApplicationConfig extends ResourceConfig {
    public MyApplicationConfig () {
        packages("ehealth");
    }
}
