package ehealth.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the measuredefaultrange database table.
 * 
 */
@Entity
@Table(name="measuredefaultrange")
@NamedQuery(name="Measuredefaultrange.findAll", query="SELECT m FROM Measuredefaultrange m")
public class Measuredefaultrange implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MeasuredefaultrangePK id;

	private int alarmLevel;

	@Column(nullable=false)
	private double endValue;

	@Column(nullable=false, length=45)
	private String rangeName;

	@Column(nullable=false)
	private double startValue;

	//bi-directional many-to-one association to Measuredefinition
	@ManyToOne
	@JoinColumn(name="MDid", nullable=false, insertable=false, updatable=false)
	private Measuredefinition measuredefinition;

	public Measuredefaultrange() {
	}

	public MeasuredefaultrangePK getId() {
		return this.id;
	}

	public void setId(MeasuredefaultrangePK id) {
		this.id = id;
	}

	public int getAlarmLevel() {
		return this.alarmLevel;
	}

	public void setAlarmLevel(int alarmLevel) {
		this.alarmLevel = alarmLevel;
	}

	public double getEndValue() {
		return this.endValue;
	}

	public void setEndValue(double endValue) {
		this.endValue = endValue;
	}

	public String getRangeName() {
		return this.rangeName;
	}

	public void setRangeName(String rangeName) {
		this.rangeName = rangeName;
	}

	public double getStartValue() {
		return this.startValue;
	}

	public void setStartValue(double startValue) {
		this.startValue = startValue;
	}

	public Measuredefinition getMeasuredefinition() {
		return this.measuredefinition;
	}

	public void setMeasuredefinition(Measuredefinition measuredefinition) {
		this.measuredefinition = measuredefinition;
	}

}