package ehealth.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the caregiver database table.
 * 
 */
@Entity
@Table(name="caregiver")
@NamedQuery(name="Caregiver.findAll", query="SELECT c FROM Caregiver c")
public class Caregiver implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int CGid;

	//bi-directional many-to-one association to Userprofile
	@ManyToOne
	@JoinColumn(name="Care_UPid", nullable=false)
	private Userprofile userprofile1;

	//bi-directional many-to-one association to Userprofile
	@ManyToOne
	@JoinColumn(name="UPid", nullable=false)
	private Userprofile userprofile2;

	public Caregiver() {
	}

	public int getCGid() {
		return this.CGid;
	}

	public void setCGid(int CGid) {
		this.CGid = CGid;
	}

	public Userprofile getUserprofile1() {
		return this.userprofile1;
	}

	public void setUserprofile1(Userprofile userprofile1) {
		this.userprofile1 = userprofile1;
	}

	public Userprofile getUserprofile2() {
		return this.userprofile2;
	}

	public void setUserprofile2(Userprofile userprofile2) {
		this.userprofile2 = userprofile2;
	}

}