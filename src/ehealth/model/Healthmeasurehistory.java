package ehealth.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the healthmeasurehistory database table.
 * 
 */
@Entity
@Table(name="healthmeasurehistory")
@NamedQuery(name="Healthmeasurehistory.findAll", query="SELECT h FROM Healthmeasurehistory h")
public class Healthmeasurehistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int HMHid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date dateTime;

	@Column(nullable=false)
	private double value;

	//bi-directional many-to-one association to Measuredefinition
	@ManyToOne
	@JoinColumn(name="MDid", nullable=false)
	private Measuredefinition measuredefinition;

	//bi-directional many-to-one association to Userprofile
	@ManyToOne
	@JoinColumn(name="UPid", nullable=false)
	private Userprofile userprofile;

	public Healthmeasurehistory() {
	}

	public int getHMHid() {
		return this.HMHid;
	}

	public void setHMHid(int HMHid) {
		this.HMHid = HMHid;
	}

	public Date getDateTime() {
		return this.dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public double getValue() {
		return this.value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Measuredefinition getMeasuredefinition() {
		return this.measuredefinition;
	}

	public void setMeasuredefinition(Measuredefinition measuredefinition) {
		this.measuredefinition = measuredefinition;
	}

	public Userprofile getUserprofile() {
		return this.userprofile;
	}

	public void setUserprofile(Userprofile userprofile) {
		this.userprofile = userprofile;
	}

}