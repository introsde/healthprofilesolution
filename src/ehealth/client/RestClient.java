package ehealth.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ehealth.model.Goalsetting;
import ehealth.model.Userprofile;

public class RestClient {

	public static void main(String[] args) throws Exception {

		ClientConfig config = new ClientConfig();

		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(UriBuilder.fromUri(
				"http://localhost:8001/rest/healthsolutions/").build());

		System.out.println("==========Step 1: Checking login==========");

		WebTarget path = target.path("/rest/checklogin")
				.queryParam("username", "Pogba.Paul")
				.queryParam("token", "551a61b3-eaee-4eb3-a838-d14c2b9ee582");

		Builder req = path.request().accept(MediaType.APPLICATION_JSON);

		// String loginRequest = "{username:" + "Pogba.Paul" + ",token:" +
		// "551a61b3-eaee-4eb3-a838-d14c2b9ee582" + "}";
		//
		// String login = "<xml><username>" + "Pogba.Paul" +
		// "</username><token>" + "551a61b3-eaee-4eb3-a838-d14c2b9ee582" +
		// "</token></xml>";

		Response res = req.get(Response.class);

		// Response res1 = req.post(Entity.json(loginRequest));

		// int d = Integer.parseInt();

		String responseString = res.readEntity(String.class);
		if ((res.getStatus() == 200 || res.getStatus() == 201 || res
				.getStatus() == 202)) {
			// if (d != 0) {
			System.out.println(responseString);
			// }
		}

		WebTarget pathProfile = target.path("/rest/profile")
				.queryParam("username", "Pogba.Paul")
				.queryParam("token", "551a61b3-eaee-4eb3-a838-d14c2b9ee582");

		Builder reqProfile = pathProfile.request().accept(
				MediaType.APPLICATION_JSON);

		Response resProfile = reqProfile.get(Response.class);

		Userprofile userProfile = null;
		if ((resProfile.getStatus() == 200)) {
			userProfile = resProfile.readEntity(Userprofile.class);
			System.out.println(userProfile);

			WebTarget pathGoal = target
					.path("/rest/goal")
					.queryParam("username", "Pogba.Paul")
					.queryParam("token", "551a61b3-eaee-4eb3-a838-d14c2b9ee582")
					.queryParam("userid", userProfile.getUPid());

			Builder reqGoal = pathGoal.request().accept(
					MediaType.APPLICATION_JSON);

			Response resGoal = reqGoal.get(Response.class);

			if (resGoal.getStatus() == 200) {
				Goalsetting goal = resGoal.readEntity(Goalsetting.class);
				System.out.println(goal);
			}
		}

	}
}