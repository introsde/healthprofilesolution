package ehealth.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * <p>
 * HealthPRofileSolutionDAO works with persistence to access the database in
 * efficient way.
 * <p>
 * 
 * @author Jasim
 *
 */
public enum HealthProfileSolutionDAO {
	instance;
	private EntityManagerFactory emf;

	private HealthProfileSolutionDAO() {
		if (emf != null) {
			emf.close();
		}
		emf = Persistence.createEntityManagerFactory("EhealthPU");
	}

	public EntityManager createEntityManager() {
		try {
			return emf.createEntityManager();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void closeConnections(EntityManager em) {
		em.close();
	}

	public EntityTransaction getTransaction(EntityManager em) {
		return em.getTransaction();
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return emf;
	}
}
